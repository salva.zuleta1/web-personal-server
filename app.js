const bodyParser = require("body-parser");
const express = require("express");

const app = express();
const { API_VERSION } = require("./config");

//load routings
const authRoutes = require('./routes/auth.js');
const userRoutes = require("./routes/user");
const menuRoutes = require("./routes/menu");
const newsletterRoutes = require("./routes/newsletter");
const courseRoutes = require("./routes/course");
const postRoutes = require("./routes/post");


app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//configure header http
// Configure Header HTTP
app.use((req, res, next) => {
  res.header("Access-Control-Allow-Origin", "*");
  res.header(
    "Access-Control-Allow-Headers",
    "Authorization, X-API-KEY, Origin, X-Requested-With, Content-Type, Accept, Access-Control-Allow-Request-Method"
  );
  res.header("Access-Control-Allow-Methods", "GET, POST, OPTIONS, PUT, DELETE");
  res.header("Allow", "GET, POST, OPTIONS, PUT, DELETE");
  next();
});

//Router basic
app.use(`/api/${API_VERSION}`, authRoutes);
app.use(`/api/${API_VERSION}`, userRoutes);
app.use(`/api/${API_VERSION}`, menuRoutes);
app.use(`/api/${API_VERSION}`, newsletterRoutes);
app.use(`/api/${API_VERSION}`, courseRoutes);
app.use(`/api/${API_VERSION}`, postRoutes);

module.exports = app;
